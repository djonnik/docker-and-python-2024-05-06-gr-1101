from psycopg import connect, OperationalError

from bot.src.settings import settings


def create_connection():
    try:
        conn = connect(
            dbname=settings.POSTGRES_NAME,
            user=settings.POSTGRES_USER,
            password=settings.POSTGRES_PASSWORD,
            host=settings.POSTGRES_HOST,
            port=settings.POSTGRES_PORT_HOST
        )
        print("Connection to PostgreSQL DB successful")
        return conn
    except OperationalError as e:
        print(f"Error: {e}")
        return None


conn = create_connection()

